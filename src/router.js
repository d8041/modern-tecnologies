import { createWebHistory, createRouter } from "vue-router";
import Home from "./components/Home.vue";
import Login from "./components/Login.vue";
import Register from "./components/Register.vue";
const Profile = () => import("./components/Profile.vue")
// const BoardAdmin = () => import("./components/BoardAdmin.vue")
// const BoardModerator = () => import("./components/BoardModerator.vue")
const SearchBook = () => import("./components/SearchBook.vue")
const BoardAdmin = () => import("./components/BoardAdmin.vue")
const SearchReader = () => import("./components/SearchReader.vue")
const BookCard = () => import("./components/BookCard.vue")
const LendingBooks = () => import("./components/LendingBooks.vue")
const routes = [
  
  {
    path: "/",
    name: "home",
    component: Home,
  },
  {
    path: "/home",
    component: Home,
  },
  {
    path: "/login",
    component: Login,
  },
  {
    path: "/register",
    component: Register,
  },
  {
    path: "/profile",
    name: "profile",
    component: Profile,
  },
  {
    path: "/admin",
    name: "admin",

    component: BoardAdmin,
  },
  {
    path: "/reader",
    name: "reader",

    component: SearchReader,
  },
  {
    path: "/search",
    name: "search",
    component: SearchBook,
  },
  {
    path: "/book",
    name: "book",
    component: BookCard,
  },
  {
    path: "/lending_books",
    name: "lending_books",
    component: LendingBooks,
  },
];
const router = createRouter({
  history: createWebHistory(),
  routes,
});
export default router;