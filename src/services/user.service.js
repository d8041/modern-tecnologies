import axios from 'axios';
import authHeader from './auth-header';
const API_URL = 'http://89.108.77.46:3000/api/';
class UserService {
  getUsers() {
    return axios.get(API_URL + 'user', { headers: authHeader() });
  }
  getUserRole() {
    return axios.get(API_URL + 'user_role', { headers: authHeader() });
  }
  getRole() {
    return axios.get(API_URL + 'role', { headers: authHeader() });
  }
  putUserRole(id, value) {
    const body = {
      "id_user": id,
      "id_role": value
    }
    return axios.put(API_URL + 'user_role/' + id, body, { headers: authHeader() });
  }
}
export default new UserService();//http://89.108.77.46/