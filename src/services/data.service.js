import axios from 'axios';
import authHeader from './auth-header';
const API_URL = 'http://89.108.77.46:3000/api/';
class DataService {
  getBooks() {
    return axios.get(API_URL + 'book', { headers: authHeader() });
  }
  getBooks(category) {
    return axios.get(API_URL + 'book/?id_category=' + category, { headers: authHeader() });
  }
  getBook(nam) {
    return axios.get(API_URL + 'book/' + nam, { headers: authHeader() });
  }
  getCategory() {
    return axios.get(API_URL + 'category', { headers: authHeader() });
  }
  getAuthor() {
    return axios.get(API_URL + 'author', { headers: authHeader() });
  }
  getAuthorBook(id_author) {
    return axios.get(API_URL + 'author_book/?id_author=' + id_author, { headers: authHeader() });
  }
  getReaders(){
    return axios.get(API_URL + 'reader', { headers: authHeader() });
  }
  getActionWithBooks(id_reader){
    return axios.get(API_URL + 'action_with_books/?id_reader=' + id_reader, { headers: authHeader() });
  }
  postActionWithBook(action_with_book){
    return axios.post(API_URL + 'action_with_books', action_with_book, { headers: authHeader() });
  }
  getActionWithBooks(){
    return axios.get(API_URL + 'action_with_books', { headers: authHeader() });
  }
  getAction(){
    return axios.get(API_URL + 'action', { headers: authHeader() });
  }
  deleteActionWithBooks(nam) {
    return axios.delete(API_URL + 'action_with_books/' + nam, { headers: authHeader() });
  }
}
export default new DataService();//