import axios from 'axios';
const API_URL = 'http://89.108.77.46:3000/api/user/';
class AuthService {
  login(user) {
    return axios
      .post(API_URL + 'login', {
        login: user.login,
        password: user.password
      })
      .then(response => {
        if (response.data.token) {
          localStorage.setItem('user', JSON.stringify(response.data));
        }
        return response.data;
      });
  }
  logout() {
    localStorage.removeItem('user');
  }
  register(user) {
    return axios.post(API_URL + 'registration', {
      login: user.login,
      name: user.name,
      surname: user.surname,
      patronymic: user.patronymic,
      password: user.password
    });
  }
}
export default new AuthService();